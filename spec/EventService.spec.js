import {ExpressDataApplication} from '@themost/express';
import {
    DataConfigurationStrategy,
    DefaultSchemaLoaderStrategy, 
    SchemaLoaderStrategy
} from '@themost/data';
import {OAuth2ClientService} from '@universis/api/dist/server/services/oauth2-client-service';
import {EventService} from '../lib/src/EventService';
import {RandomUtils} from '@themost/common/utils';
import request from 'supertest';
import { TestUtils } from '@universis/api/dist/server/utils';


describe('EventService', () => {
    /**
     * @type {import('express').Application}
     */
    let app;
    /**
     * @type {ExpressDataApplication}
     */
    let application;
    // noinspection JSValidateJSDoc
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(() => {
        app = require('@universis/api/dist/server/app');
        /**
         * @type {ExpressDataApplication}
         */
        application = app.get(ExpressDataApplication.name);
        const configuration=application.getConfiguration();
        configuration.setSourceAt('settings/schema/loaders', [
            {
                loaderType: '@universis/events#EventSchemaLoader'
            }
        ]);
        // reload SchemaLoaderStrategy strategy
        configuration.useStrategy(SchemaLoaderStrategy, DefaultSchemaLoaderStrategy);
        application.useModelBuilder();

    });

    beforeEach(async () => {
        context = application.createContext();
    });

    afterEach(done => {
        // important: clear cache after each test
        const configuration = context.getConfiguration();
        if (Object.prototype.hasOwnProperty.call(configuration, 'cache')) {
            delete configuration.cache;
        }
        context.finalize(() => {
            return done();
        });
    });


    it('should create instance', () => {
        const service = new EventService(application);
        expect(service).toBeTruthy();
    });

    it('should install service', () => {
        /**
         * @type {DataConfigurationStrategy}
         */
        const dataConfiguration = application.getConfiguration().getStrategy(DataConfigurationStrategy);
        // expect UserLoginAction to be undefined
        let model = dataConfiguration.getModelDefinition('Event');
        expect(model).toBeTruthy();

    });


    it('should use EventStatusType.getItems()', async () => {
        const items = await context.model('EventStatusType').asQueryable().silent().getItems();
        expect(items.length).toBeGreaterThan(0);

    });

    it('should GET /api/events', async () => {
        // use
        // service
        if (application.hasService(EventService) === false) {
            application.useService(EventService)
        }
        const user = {
            name: 'registrar1@example.com',
            authenticationScope: 'registrar',
            authenticationToken: RandomUtils.randomChars(48),
            authenticationType: 'Bearer'
        };
        // spy on OAuth2ClientService.getTokenInfo()
        let client = application.getStrategy(OAuth2ClientService);
        spyOn(client, 'getTokenInfo').and.returnValue(new Promise(resolve => {
            return resolve({
                active: true,
                username: user.name,
                scope: 'registrar'
            });
        }));
        // request data
        let response = await request(app)
            .get('/api/events')
            .set('Authorization', `Bearer ${user.authenticationToken}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        expect(response.status).toBe(200);
    });

    it('should save event', async () => {
        // set context user as admin
        context.user = {
            name: 'registrar1@example.com',
            authenticationScope: 'registrar',
            authenticationToken: RandomUtils.randomChars(48),
            authenticationType: 'Bearer'
        };
        // spy on OAuth2ClientService.getTokenInfo()
        let client = application.getStrategy(OAuth2ClientService);
        spyOn(client, 'getTokenInfo').and.returnValue(new Promise(resolve => {
            return resolve({
                active: true,
                username: context.user.name,
                scope: 'registrar'
            });
        }));
        // set new itens
        const item = {
            "name": "Maths",
            "startDate": new Date("2020-04-01 09:00:00"),
            "endDate": new Date("2020-04-01 11:00:00")
        };
        // executes in test transaction
        await TestUtils.executeInTransaction(context, async() => {
            // save new item
            await context.model('Event').save(item);
            // validate id
            expect(item.id).toBeInstanceOf(Number);

            // get item again
            const event = await context.model('Event').where('id').equal(item.id).getItem();
            // validate object
            expect  (event).toBeTruthy();
            // validate object
            expect  (event.eventStatus.alternateName).toBe('EventOpened');
        });
    });

    it('should save event hours specification', async () => {
        // set context user as admin
        context.user = {
            name: 'registrar1@example.com',
            authenticationScope: 'registrar',
            authenticationToken: RandomUtils.randomChars(48),
            authenticationType: 'Bearer'
        };
        // spy on OAuth2ClientService.getTokenInfo()
        let client = application.getStrategy(OAuth2ClientService);
        spyOn(client, 'getTokenInfo').and.returnValue(new Promise(resolve => {
            return resolve({
                active: true,
                username: context.user.name,
                scope: 'registrar'
            });
        }));
        // set new itens
        const item = {
            "name": "Tue 3rd hour",
            "validFrom": new Date("2020-04-01 00:00:00"),
            "validThrough": new Date("2020-04-30 00:00:00"),
            "duration": "PT45M",
            "minuteOfHour": "15",
            "hourOfDay": "10",
            "dayOfWeek": "2"
        };
        // executes in test transaction
        await TestUtils.executeInTransaction(context, async() => {
            // save new item
            await context.model('EventHoursSpecification').save(item);
            // validate id
            expect(item.id).toBeInstanceOf(Number);

            // get item again
            const eventHoursSpecication = await context.model('EventHoursSpecification').where('name').equal('Tue 3rd hour').getItem();
            // validate object
            expect  (eventHoursSpecication).toBeTruthy();
        });
    });
});
