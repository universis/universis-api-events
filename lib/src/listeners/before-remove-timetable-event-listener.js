import { DataEventArgs } from "@themost/data";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
	return beforeRemoveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

/**
 * @param {DataEventArgs} event
 */
export async function beforeRemoveAsync(event) {
	if (event.state === 4) {
		const context = event.model.context;

		// delete copy actions associated with the timetable
		const copyActions = await context.model('CopyTimetableEventAction')
			.where('timetableEvent').equal(event.target.id)
			.or('result').equal(event.target.id)
			.select('id')
			.getItems();

		for (const copyAction of copyActions) {
			await context.model('CopyTimetableEventAction').remove(copyAction);
		}

		// delete events associated with the timetable
		const timetableEvents = await context.model('Events')
			.where('superEvent').equal(event.target.id)
			.or('superEvent/superEvent').equal(event.target.id)
			.select(['id', 'additionalType', 'superEvent'])
			.take(-1)
			.getItems();

		// move sub-events to the beginning of the array so that they are deleted first
		for (let i = 0; i < timetableEvents.length; i++) {
			if (timetableEvents[i].superEvent !== event.target.id) {
				timetableEvents.unshift(timetableEvents.splice(i, 1)[0]);
			}
		}

		for (const event of timetableEvents) {
			await context.model(event.additionalType).remove(event)
		}
	}
}
