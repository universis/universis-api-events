/**
 * @class
 * @property {boolean} success
 * @property {string} code
 * @property {number} statusCode
 * @property {string} message
 * @property {string} innerMessage
 * @property {Array<any>} data
 * @property {Array<ValidationResult>} validationResults
 */
export class ValidationResult  {
    /**
     * @param {boolean} success
     * @param {string=} code
     * @param {string=} message
     * @param {string=} innerMessage
     * @param {any=} data
     */
    constructor(success, code, message, innerMessage, data) {
        Object.defineProperty(this, 'type', {
            value: this.constructor.name,
            writable: true,
            enumerable: true
        });
        this.success = typeof success !== 'undefined' && success !== null ? success : false;
        this.statusCode = this.success ? 200 : 422;
        this.code = code;
        this.message = message;
        this.innerMessage = innerMessage;
        this.data = data;
    }
}
