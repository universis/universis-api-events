import {EdmMapping, DataObject} from '@themost/data';
import { parseExpression } from 'cron-parser';
import moment from 'moment'

/**
 * @class
 
 * @property {number} about
 * @property {Audience|any} audience
 * @property {EventHoursSpecification|any} eventHoursSpecification
 * @property {number} remainingAttendeeCapacity
 * @property {Date} startDate
 * @property {Date} endDate
 * @property {Date} doorTime
 * @property {User} contributor
 * @property {number} maximumAttendeeCapacity
 * @property {Department} organizer
 * @property {Array<User>} attendees
 * @property {string} inLanguage
 * @property {EventStatusType|any} eventStatus
 * @property {Event} superEvent
 * @property {string} duration
 * @property {Date} previousStartDate
 * @property {Place|any} location
 * @property {boolean} isAccessibleForFree
 * @property {User} performer
 * @augments {DataObject}
 */
@EdmMapping.entityType('Event')
class Event extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    async extendRecurringEventPeriod(oldEHS, newEHS) {
        const event = await this.context.model('Event').where('id').equal(this.getId()).getItem();
        const typedEvent = await this.context.model(event.additionalType).where('id').equal(this.getId()).getItem();
        const typedEHS = await this.context.model('EventHoursSpecification').where('id').equal(newEHS.id).silent().getTypedItem();
        
        let cronjob = typedEHS.toCronJobString();
        let options = {
            // When currentDate matches given cron expression, cron-parser will miss current event
            // Subtract one second from the currentDate to avoid this
            // https://github.com/harrisiirak/cron-parser/issues/289
            currentDate: new Date(new Date(newEHS.validFrom) - 1000),
            endDate: newEHS.validThrough,
            iterator: true
        };
        let interval = parseExpression(cronjob, options);
        let intervals = [];
        while (interval) {
            try {
                let obj = interval.next();
                intervals.push(obj.value);
            } catch (err) {
                break;
            }
        }

        let exceptDates;
        const { id, dateCreated, dateModified, ..._event } = typedEvent;
        const timetable = await this.context.model('TimetableEvent').where('id').equal(_event.superEvent).getItem();
        if (timetable) {
            exceptDates = await this.context.model('ExceptHoursSpecification').where('academicYear').equal(timetable.academicYear).expand('eventExceptStatus').getAllItems();
        }

        if (newEHS.validFrom > oldEHS.validFrom || newEHS.validThrough < oldEHS.validThrough) {
            throw new Error('Can\'t shrink the time-frame.')
        }

        const events = []
        for (const interval of intervals) {
            const startDate = interval.toDate();
            const endDate = moment(interval.toDate()).add(moment.duration(newEHS.duration)).toDate();

            // skip old sub-events
            if (startDate >= oldEHS.validFrom && endDate <= oldEHS.validThrough) {
                continue;
            } 

            const event = {
                ..._event,
                startDate,
                endDate,
                superEvent: id,
                eventHoursSpecification: null // sub-events shouldn't have eventHoursSpecification
            }

            if (exceptDates) {
                const exceptDate = Event.getExceptDate(event.startDate, event.endDate, exceptDates);

                // don't create sub-events for except dates according to the timetable's settings
                if (exceptDate && !timetable.createExceptEvents) {
                    continue;
                }

                // create sub-events with specific status according to the except date's settings
                if (exceptDate && exceptDate.updateEventStatus) {
                    event.eventStatus = exceptDate.eventExceptStatus;
                }
            }

            events.push(event);
        }
        
        await this.context.model(typedEvent.additionalType).silent().save(events);
    }

    static calculateDatesByTimeFrame(event, newTimeFrame, timeFrame) {
        const firstDayFirstWeek = moment(timeFrame.startDate).startOf('week');
        const startDate = moment(event.eventHoursSpecification ? event.eventHoursSpecification.validFrom : event.startDate);
        const startWeek = Math.floor(moment.duration(startDate.diff(firstDayFirstWeek)).asWeeks());

        // let the new startDate of the event be the startDate of the  new time frame and shift it accordingly
        const newStartDate = moment(newTimeFrame.startDate);
        if (startWeek === 0 && startDate.day() < newStartDate.day()) {
            //  The new time frame starts later in the week than when the event is supposed to start (e.g. the old time frame started on
            //  Mon and the event was on Wed but the new timetable starts on Thu) and therefore the event can't start on the same day of the first week
            // in the new timetable. If the event is a recurring one, sub-events for it should be created from the new timetable's start date and on (startDate isn't shifted).
            // Else it should be moved on the same day of the next week.
            if (!event.eventHoursSpecification) { newStartDate.add(1, 'week').day(startDate.day()); }
        } else {
            // If the new time frame starts earlier in the first week than when the event is supposed to start or the event is supposed to start on any week
            // other than the first, the event (recurring or not) should start on the same week and day as in the old time frame.
            newStartDate.add(startWeek, 'weeks').day(startDate.day());
        }

        return Event.calculateDatesByDate(event, newStartDate);
    }

    static calculateDatesByDate(event, newStart) {
        const getNewDates = (startDate, endDate) => {
            const start = moment(startDate);
            const end = moment(endDate);
            const weekSpan = Math.floor(moment.duration(end.diff(moment(start).startOf('week'))).asWeeks());

            const newStartDate = moment(newStart).hours(start.hours()).minutes(start.minutes());
            const newEndDate = moment(newStartDate).add(weekSpan, 'weeks').day(end.day()).hours(end.hours()).minutes(end.minutes());

            return {
                startDate: newStartDate.toDate(),
                endDate: newEndDate.toDate()
            }
        }

        const getMonthsBetweenDates = (startDate, endDate) => {
            // find difference in months
            const difference = (endDate.getFullYear() * 12 + endDate.getMonth()) - (startDate.getFullYear() * 12 + startDate.getMonth()) + 1;

            const months = [];
            let startDateMonth = startDate.getMonth();

            // add months from startMonth and reset startMonth with the modulo operator when it exceeds December
            for (let i = 0; i < difference; i++) {
                months.push(startDateMonth >= 12 ? startDateMonth % 12 + 1 : startDateMonth + 1);
                startDateMonth += 1;
            }

            // remove duplicate months and sort them
            return Array.from(new Set(months)).sort((a, b) => a - b);
        }

        event = JSON.parse(JSON.stringify(event))
        if (event.eventHoursSpecification) {
            const newDates = getNewDates(event.eventHoursSpecification.validFrom, event.eventHoursSpecification.validThrough)
            event.startDate = event.eventHoursSpecification.validFrom = newDates.startDate;
            event.endDate = event.eventHoursSpecification.validThrough = newDates.endDate;
            delete event.eventHoursSpecification.id
            delete event.eventHoursSpecification.identifier

            // opens, closes are Time fields not DateTime fields
            const toTimeString = value => value instanceof Date ? value.toTimeString().substring(0, 8) : value
            event.eventHoursSpecification.opens = toTimeString(event.eventHoursSpecification.opens);
            event.eventHoursSpecification.closes = toTimeString(event.eventHoursSpecification.closes);
            
            // months may have changed, recalculate them and join them to a cronString.
            event.eventHoursSpecification.monthOfYear = getMonthsBetweenDates(newDates.startDate, newDates.endDate).join(',');
        } else {
            const newDates = getNewDates(event.startDate, event.endDate)
            event.startDate = newDates.startDate;
            event.endDate = newDates.endDate;
        }

        return event
    }

    static getExceptDate(startDate, endDate, exceptDates = []) {
        const isBetween = (date, dateRange) => date > dateRange.validFrom && date < dateRange.validThrough

        for (const exceptDate of exceptDates) {
            if (isBetween(startDate, exceptDate) || isBetween(endDate, exceptDate)) {
                return exceptDate;
            }
        }
    }
}
module.exports = Event;
