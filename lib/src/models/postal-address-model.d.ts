import ContactPoint = require("./contact-point.model");

declare class PostalAddress extends ContactPoint {}

export = PostalAddress;
