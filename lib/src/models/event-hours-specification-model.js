import {EdmMapping, DataObject} from '@themost/data';

/**
 * @class
 * @property {number} id
 * @property {string} additionalType
 * @property {string} alternateName
 * @property {string} description
 * @property {string} image
 * @property {string} name
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User} createdBy
 * @property {User} modifiedBy
 * @property {Date} validFrom
 * @property {Date} validThrough
 * @property {Duration} opens
 * @property {number} duration
 * @property {Duration} closes
 * @property {string} minuteOfHour
 * @property {string} hourOfDay
 * @property {string} dayOfMonth
 * @property {string} monthOfYear
 * @property {string} dayOfWeek
 * @augments {DataObject}
 */
@EdmMapping.entityType('EventHoursSpecification')
class EventHoursSpecification extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
    
    toCronJobString() {
        return (this.minuteOfHour || "*") + " " +
            (this.hourOfDay || "*") + " " +
            (this.dayOfMonth || "*") + " " +
            (this.monthOfYear || "*") + " " +
            (this.dayOfWeek || "*");
    }
    
}

module.exports = EventHoursSpecification;
